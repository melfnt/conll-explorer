
SHELL=/bin/bash

PYFILES=*.py
DOCFILES=${PYFILES:.py=.html}


cleandoc:
	-rm -f docs/*.html

doc: cleandoc
	mkdir -p docs
	# pydoc3 requires a / in the argument in order to interpret it as a filename
	cd src; pydoc3 -w ./$(PYFILES); \
	for f in $(DOCFILES) ; do \
	 mv $$f ../docs/src.$$f ; \
	done
	pydoc3 -w src
	mv src.html docs/index.html
	
.PHONY: doc cleandoc
