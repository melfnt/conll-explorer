#
# example2.py
# performs three queries at the same time over an italian corpus, searching:
#  - all the verbs
#  - all the nouns
#  - all the v-pp i.e. all the triplets formed by a noun, that depends on a conjunction, that depends on a verb.
#
# here is a graphical representation of the tree of a v-pp:
#
#           v (must be a verb)
#           |
#           |
#           e (must be a conjunction)
#           |
#           |
#           s (must be a noun)
#
# then, it writes the result in tsv format, writing three different files:
#  - the first one contains all the lemmas of all the verbs present in the corpus, one for each row
#  - the second one contains all the lemmas of all the nouns present in the corpus, one for each row
#  - the last one has a row for every v-pp found: each row has three columns, separated by a '\t' character: the lemma of the verb (v), followed by the lemma of the conjunction (e), followed by the lemma of the noun (s).
#
#


from TokenShape import TokenShape
from Corpus import Corpus

if __name__ == "__main__":
	
	# Corpus object creation
	corpus = Corpus (open ("../corpora/itwac_sample"), sentence_separator_line="#\n", comment_line_character="#"  )
	
	# TokenShape definition for the first query: a must have UPOSTAG=V (it must be a verb)
	a = TokenShape ( specified_attributes_dict={"UPOSTAG":"V"} )
	
	# TokenShape definition for the second query: b must be a noun
	b = TokenShape ( specified_attributes_dict={"UPOSTAG":"S"} )
	
	# TokenShape definition for the third query: v must be a verb, e must be a conjunction and s must be a noun
	v = TokenShape ( specified_attributes_dict={"UPOSTAG":"V"} )
	e = TokenShape ( specified_attributes_dict={"UPOSTAG":"E"} )
	s = TokenShape ( specified_attributes_dict={"UPOSTAG":"S"} )
	
	
	# tree structure definition for the third query: s depends on e that depends on v
	s.depends ( e )
	e.depends ( v )
	
	# executing the query
	results_list = corpus.find_all ( [a,b,v] )
	
	# writing the results 
	results_list[0].header("VERBS").write_columns ( "example2_results_verbs.tsv", tokens_to_print=[(a, ["LEMMA"])] )
	results_list[1].header("NOUNS").write_columns ( "example2_results_nouns.tsv", tokens_to_print=[(b, ["LEMMA"])] )
	results_list[2].header("V-PP").write_columns ( "example2_results_vpp.tsv", tokens_to_print=[(v, ["LEMMA"]),(e, ["LEMMA"]),(s, ["LEMMA"])] )
	
