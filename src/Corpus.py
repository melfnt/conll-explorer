#!/usr/bin/python3

CoNLL_U_mapping = {
                    "ID" : 0,
				   "FORM": 1,
 				   "LEMMA": 2,
				   "UPOSTAG": 3,
				   "XPOSTAG": 4,
				   "FEATS": 5,
				   "HEAD": 6,
				   "DEPREL": 7,
				   "DEPS": 8,
				   "MISC": 9,
			      }

import QueryResult
import CompiledTokenShape
import TokenShape
	
class Matcher:

	def __init__ ( self, head_col, deprel_col  ):
		self.head_col = head_col
		self.deprel_col = deprel_col

	#TODO: optimizable code
	def try_match_tree ( self, root, parent_id, relation_type, sent, possible_choices, check_dependency=True ):
		
		#~ print ()
		#~ print ("[DEBUG] try_match_tree root node: {}".format (root))
		#~ print ("[DEBUG] parent_id: {}".format (parent_id))
		
		results = QueryResult.CompactChoiceNode ()
		
		for c in possible_choices[root]:
			#~ print ("[DEBUG] trying c={}".format (c))
			#~ print ("[DEBUG] conll token: {}".format (sent[c]))
			
			if ( not check_dependency ) or ( int(sent[c][self.head_col]) == parent_id and ( relation_type is None or sent[c][self.deprel_col] == relation_type) ):
				
				#~ print ("[DEBUG] good dependency")
				#base case: root is a leaf, so c is a successful match
				if not root.children:
					results.add_choice ( QueryResult.CompactMatchedSentence ( (root.source, c), None ) )
					#~ print ("[DEBUG] it was a leaf, c={} is added to the results".format(c))
				else:
					#~ print ("[DEBUG] it was not a leaf, testing children...")
					children_results = []
					c_is_valid = True
					for child, relation in root.children.items():
						child_res = self.try_match_tree ( child, c+1, relation, sent, possible_choices )
						#if a children does not have a successfull match for this value of c, this value of c is not a successfull match
						if child_res.empty():
							#~ print ("[DEBUG] there is one non valid child: switching to next value of c...")
							#~ print ("")
							c_is_valid = False
							break
						children_results.append ( child_res )
					
					if c_is_valid:
						result = QueryResult.CompactMatchedSentence ( (root.source, c), children_results )
						#~ print ("[DEBUG] All the children are valid: generated solution: {}".format(result))
						results.add_choice ( result )
		
		#~ print ("[DEBUG] results: {}".format (results))
		return results
		
		
	def match_sentence ( self, sent, query ):
		
		involved = query.CompiledTokenShape_involved_in_this_query
		possible_choices = { shape:[] for shape in involved }
		root = query.root
		
		for shape in involved:
			
			matches_at_least_one = False
			
			for i in range (len(sent)):
				if shape.match ( sent[i] ):
					possible_choices[shape].append(i)
					matches_at_least_one = True
			
			#~ print ("[DEBUG] matching all the conll tokens with token {}".format(shape))
			#~ print ("[DEBUG] possible choices: {}".format (possible_choices[shape]))
			
			#this TokenShape does not match any CoNLL token
			if not matches_at_least_one:
				return []
		
		results = self.try_match_tree ( root, 0, None, sent, possible_choices, check_dependency=False )
		associations = results.expand ()
		
		return associations
	
class Corpus:
	'''
	  Objects of class Corpus incapsulates a set of sentences read from CoNLL files.
	  Each sentence is composed by a sorted sequence of CoNLL Tokens, which have some attributes that corresponds to CoNLL fields.
	  
	  This class offers a couple of way to configure how corpus will be read:
	  
	   - the character that separates the sentences can be set (default is a blank line, containing only '\n')
	   - the character that starts a comment line can be set (default is '#' )
	   - the mapping between CoNLL fields name and column indexes can be specified ( this depends on the tagset and on the CoNLL standard, default is CoNLL-U standard specified here: http://universaldependencies.org/format.html )
	    
	
	'''
	
	def __init__ ( self, input_file, sentence_separator_line="\n", comment_line_character='#', mapping=CoNLL_U_mapping ):
		'''
		  Builds an object of class Corpus.
		  This object can be used later to read a corpus file, specified by the input_file parameter.
		   input_file can be a file object (i.e. the return value of the open ("filename") function call), but pipes and similar objects can also be passed.
		   
		   the sentence_separator_line string parameter specifies which lines must be considered as sentences separators. Default is a blank line.
		   
		   the comment_line_character string parameter specifies which character starts a comment line. Default is hash "#".
		   
		   the mapping parameter specifies the mapping between CoNLL fields name and column indexes in the input file.
		    mapping must be a dictionary { "FIELD1": index1, "FIELD2", index2, ... } that is interpreted such as the the "FIELD1" field can be read from the index1-th column of the CoNLL input file and so on.
		    Indexes are zero-based:
		     the index of the "ID" field is mandatory and it is assumed to be 0 if not specified. 
		     the index of the "HEAD" field is mandatory and it is assumed to be 6 if not specified. 
		     the index of the "DEPREL" field is mandatory and it is assumed to be 7 if not specified. 
		    Field names are case-insensitive: in this way the field "LEMMA" will be treated as "Lemma" and "lemma".
		    
		    for example in the following listing:
				
				fin = open ("input.conll")
				mapping_dict = {"ID":0, "LEMMA":2, "FORM":1}
				corpus = Corpus ( fin, mapping=mapping_dict )
		    
		    The field "FORM" of each token will be read from the 2st column of the CoNLL file (i.e. the one with index 1).
		    Both the fields "Lemma" and "LEMMA" of each token will be read from the 3rd column of the CoNLL file.
		    
		    It is possible to specify the same index for multiple fields, for example in the following listing:
		    
				fin = open ("input.conll")
				mapping_dict = {"ID":0, "LEMMA":2, "FORM":1, "FORMA":1 }
				corpus = Corpus ( fin, mapping=mapping_dict )
			
			Both the fields "FORMA" and "FORM" (and "Forma", and "Form"...) of each token will be read from the 2nd column of the CoNLL file.
			("Forma" is just the italian for "Form").
			
			By default, the CoNLL-U standard is used. The mapping is specified here: http://universaldependencies.org/format.html
			 ID : 0
			 FORM: 1
			 LEMMA: 2
			 UPOSTAG: 3
			 XPOSTAG: 4
			 FEATS: 5
			 HEAD: 6
			 DEPREL: 7
			 DEPS: 8
			 MISC: 9
		   
		'''
		
		self.corpus_iterator = CorpusIterator ( input_file, sentence_separator_line, comment_line_character )
		self.mapping = mapping
		head_col = 6 if "HEAD" not in mapping else mapping["HEAD"]
		id_col = 0 if "ID" not in mapping else mapping["ID"]
		deprel_col = 7 if "DEPREL" not in mapping else mapping["DEPREL"]
		self.matcher = Matcher ( head_col, deprel_col )
		
	def find_all ( self, query_list ):
		'''
		  resolves some queries, pattern-matching every sentence of the corpus against every query in the query_list.
		  query_list must be a List of TokenShape objects.
		   For every element ts of query_list, ts is considered as a node of a tree T.
		   T is formed by ts and by every other TokenShape that depends on other nodes in T, and by every other TokenShape on which a node in T depends.
		   For every query, the full tree T is searched, regardless of the position of ts in T.
		  
		  Before executing the queries, the fields specified in the corpus mapping are compared with the fields specified in the TokenShape objects in every query.
		  If a field specified in one or more TokenShape is not specified in the mapping, an error is raised.
		  If a field specified in the mapping is not specified in any TokenShape, it is safely ignored.
		  
		  Before executing the queries, the integrity of the tree T is checked.
		  That means that T cannot have cycle (i.e. a depends on b, that depends on c, that depends on d... that depends on a) 
		  
		  :return: a list of QueryResults, where the element in position i is the result of the query in position i of the parameter query_list 
		'''
		
		results = []
		compiled_queries = []
		for query in query_list:
			compiled = TokenShape.compile ( query, self.mapping )
			qr = QueryResult.QueryResult ( query, self.mapping )
			results.append ( qr )
			compiled_queries.append ( CompiledTokenShape.QueryTree ( compiled ) )
			
		for sentence in self.corpus_iterator:
			for i in range ( len(compiled_queries) ):
				associations = self.matcher.match_sentence ( sentence.tokens, compiled_queries[i] )
				if associations:
					ms = QueryResult.MatchedSentence ( sentence, associations )
					results[i].add_sentences ( ms )
				
		return results
	
	def find ( self, query ):
		'''
		  Resolves a single query. For more details on query resolution, please refer to the documentation for the find_all function
		  
		  Equivalent to
		      find_all ( [query] ) [0]
		      
		'''
		return self.find_all ( [query] )[0]
		
class Sentence:
	'''
	  Represents a ConLL sentence, formed by a list of comment lines followed by a list of token lines 
	'''
	
	def __init__ (self):
		'''
		  initializes an empty sentence
		'''
		self.comments = []
		self.tokens = []
		
	def add_comment ( self, comment_line ):
		'''
		  adds a comment line to this sentence
		'''
		self.comments.append ( comment_line )
	
	def add_token ( self, token_line_fields ):
		'''
		  adds a token line to this sentence. The paramenter is a list of token fields
		'''
		self.tokens.append ( token_line_fields )
	
	def __repr__ ( self ):
		'''
		  returns a string representation of self
		'''
		return "CoNLL sentence with comments:" + "\n" +\
			   "\n".join(self.comments) + "\n" +\
			   "and tokens:" + "\n" +\
			   "\n".join( [ "\t".join (tok) for tok in self.tokens ]) + "\n"
		

class CorpusIterator:
	'''
	  Iterates a corpus reading from a file, a pipe... and returning one sentences at time
	  CorpusIterator can be used as follows:
	  
	    fin = open ("input_file.txt")
		ci = CorpusIterator ( fin, sentence_separator_line="", comment_line_character="#" )
		for sentence in ci:
			do_something (sentence)
		
	'''
	
	def __init__ (self, input_file, sentence_separator_line="\n", comment_line_character="#" ):
		'''
		  Initializes a new object, given the object to read, the line that is used to separate sentences, and the character that starts comment lines
		'''
		self.input_file = input_file
		self.sentence_separator_line = sentence_separator_line
		self.comment_line_character = comment_line_character
	
	def __iter__ ( self ):
		'''
		  reset iteration from the beginning of the file
		'''
		self.input_file.seek (0)
		return self
	
	def __next__ ( self ):
		'''
		  returns next sentence of the corpus, if any, else it raises StopIteration
		'''
		cur_sent = Sentence ()
		empty_sentence = True
		line = self.input_file.readline ()
		
		while not line == self.sentence_separator_line and not line == "":
			line_stripped = line.strip ()
			if line_stripped[0] == self.comment_line_character:
				cur_sent.add_comment ( line_stripped[1:] )
			else:
				cur_sent.add_token ( line_stripped.split() )
			empty_sentence = False
			
			line = self.input_file.readline ()
		
		if empty_sentence:
			raise StopIteration ()
		else:
			return cur_sent

#unit tests
if __name__ == "__main__":
	
	import io
	
	conll_sentence_string = "# comment line one" + "\n" +\
							"# comment line two" + "\n" +\
							"1	Mario	mario	NN	NNP	_	2	SBJ	_	_" + "\n" + \
							"2	eats	eat	VB	VBZ	_	0	ROOT	_	_" + "\n" + \
							"3	a	a	DT	DT	_	4	NMOD	_	_" + "\n" + \
							"4	eat	eat	NN	NN	_	2	OBJ	_	_" + "\n" +  \
							"5	in	in	IN	IN	_	4	NMOD	_	_" + "\n" + \
							"6	an	a	DT	DT	_	8	NMOD	_	_" + "\n" + \
							"7	adorable	adorable	JJ	JJ	_	8	NMOD	_	_" + "\n" + \
							"8	way	way	NN	NN	_	5	PMOD	_	_" + "\n" + \
							"9	.	.	.	.	_	2	P	_	_" + "\n" +\
							"" + "\n" +\
							"# comment second sentence" + "\n" +\
							"# blank line above ends sentences" + "\n" +\
							"1	Mario	mario	NN	NNP	_	2	SBJ	_	_" + "\n" +\
							"2	eats	eat	VB	VBZ	_	0	ROOT	_	_" + "\n" +\
							"3	a	a	DT	DT	_	4	NMOD	_	_" + "\n" +\
							"4	fridge	fridge	NN	NN	_	2	OBJ	_	_" + "\n" +\
							"5	in	in	IN	IN	_	2	ADV	_	_" + "\n" +\
							"6	a	a	DT	DT	_	8	NMOD	_	_" + "\n" +\
							"7	special	special	JJ	JJ	_	8	NMOD	_	_" + "\n" +\
							"8	way	way	NN	NN	_	5	PMOD	_	_" + "\n" +\
							"9	.	.	.	.	_	2	P	_	_"

	fin = io.StringIO( conll_sentence_string )
	ci = CorpusIterator ( fin )
	for sentence in ci:
		print (sentence)
			
	corpus = Corpus ( fin )
	b = TokenShape.TokenShape ( specified_attributes_dict={"UPOSTAG":"NN"} )
	c = TokenShape.TokenShape ( specified_attributes_dict={"UPOSTAG":"DT"} )
	c.depends ( b )
	d = TokenShape.TokenShape ( specified_attributes_dict={"LEMMA":"eat"} )
	b.depends ( d, "OBJ" )
	a = TokenShape.TokenShape ( )
	a.depends ( d, "SBJ" )

	print ("b: {}".format(b))
	print ("c: {}".format(c))
	
	qr = corpus.find (b)
	print (qr)
	
	qr.write_columns ( "results.tsv", tokens_to_print = [ (a, ["LEMMA", "UPOSTAG"]) , (b, ["LEMMA"]) ] )
	
	#~ qr.write_to_conll_file ( "results.conll" )
	#~ qr.write_to_raw_text_file ( "results.txt" )
