#!/usr/bin/python3

from functools import reduce
import CompiledTokenShape
	
class QueryResult:
	'''
	  Objects of class QueryResult contains the list of occurrencies of ConLL tokens that matches a query in a Corpus.
	  
	  The results can be converted and outputted in various formats inculding CoNLL, raw text, csv.
	  Tokens that compose the results can be excluded or included from the output. Every attribute of each token can be included or excluded from the output.  
	  
	  Attributes of the tokens composing QueryResult objects can be given in input to another query.
	
	'''
	
	def __init__ ( self, original_query, mapping ):
		'''
		  Builds an empty object, starting from a query (i.e. a tree of TokenShape) and the mapping between field names and CoNLL column indexes.
		
		'''
		self.original_query = original_query
		self.results = []
		self.mapping = mapping
		self.token_sep = "\t"
		self.field_sep = "_"
		self.header_str = None
		self.sentence_separator_line_str = None
		
		all_the_tokenshapes = CompiledTokenShape.tree_to_set ( CompiledTokenShape.find_root (original_query) )
		self.tokens_to_print = [ (tok,["LEMMA","UPOSTAG"]) for tok in list ( all_the_tokenshapes ) ]
	
	def add_sentences ( self, matched_sentence ):
		'''
		  Adds a list of matched sentences to the current result list
		'''
		self.results.append ( matched_sentence )
	
	def write_to_conll_file (self, output_filename):
		'''
		  outputs the query results represented by the self object to a CoNLL file.
		
		'''
		with open (output_filename, "w") as fout:
			for matched in self.results:
				sentence = matched.sentence
				fout.write ( "\n".join( ["#"+comment for comment in sentence.comments] ) )
				fout.write ("\n")
				fout.write ( "\n".join( ["\t".join(tok) for tok in sentence.tokens] ) )
				fout.write ("\n")
		
	
	def write_to_raw_text_file ( self, output_filename, form_col=None ):
		'''
		  outputs the query results represented by the self object to a raw text file.
		  Every single sentence that matched the query is printed on a single line of the file.
		  
		'''
		form_col = form_col if form_col is not None else self.mapping["FORM"] 
		with open (output_filename, "w") as fout:
			for matched in self.results:
				sentence = matched.sentence
				fout.write ( " ".join( [tok[form_col] for tok in sentence.tokens] ) )
				fout.write ("\n")
	
	def write_columns ( self, output_filename, tokens_to_print=None, field_separator="_", token_separator="\t" ):
		
		if tokens_to_print is None:
			tokens_to_print = self.tokens_to_print
		if field_separator is None:
			field_separator = self.field_sep
		if token_separator is None:
			token_separator = self.token_sep
		
		try:
			tokens_to_print_compiled = [ (tok[0], [self.mapping[field] for field in tok[1] ] ) for tok in tokens_to_print ]
		except KeyError:
			error_msg = "[BAD USER INPUT] the specified list of tokens to print is not valid" + "\n" + \
						"                 list: {}".format (tokens_to_print) + "\n" +\
						"                 MAPPING: {}".format ( self.mapping ) + "\n" +\
						"                 note that the list above may be the default list of tokens." +"\n" +\
						"                 in that case, please provide a valid list as argument to the function write_columns" + "\n"
			raise KeyError (error_msg)
		
		with open (output_filename, "w") as fout:
			
			if self.header_str is not None:
				fout.write (self.header_str)
				fout.write ("\n")
			
			for matched in self.results:
				sentence = matched.sentence.tokens
				assocs_list = matched.associations
				
				if self.sentence_separator_line_str is not None:
					fout.write (self.sentence_separator_line_str)
					fout.write ("\n")
				
				for assoc in assocs_list:
					first = True
					for tok_to_print in tokens_to_print_compiled:
						if first:
							first = False
						else:
							fout.write ( token_separator )
						
						token_idx = assoc[ tok_to_print[0] ]
						token = sentence[ token_idx ]
						fields = [ token[i] for i in tok_to_print[1] ]
						
						fout.write ( field_separator.join (fields) )
						
					fout.write ("\n")
					
	def write_custom_format ( self, output_filename, str_format, *tokens_to_print ):
		
		tokens_to_print_list = list ( tokens_to_print )
		
		try:
			tokens_to_print_compiled = [ (tok[0], self.mapping[tok[1]] ) for tok in tokens_to_print_list ]
		except KeyError:
			error_msg = "[BAD USER INPUT] the specified list of tokens to print is not valid" + "\n" + \
						"                 list: {}".format (tokens_to_print) + "\n" +\
						"                 MAPPING: {}".format ( self.mapping ) + "\n"
			raise KeyError (error_msg)
		
		with open (output_filename, "w") as fout:
			
			if self.header_str is not None:
				fout.write (self.header_str)
				fout.write ("\n")
			
			for matched in self.results:
				sentence = matched.sentence.tokens
				assocs_list = matched.associations
				
				if self.sentence_separator_line_str is not None:
					fout.write (self.sentence_separator_line_str)
					fout.write ("\n")
				
				for assoc in assocs_list:
					
					args = []
					for tok_to_print in tokens_to_print_compiled:
						token_idx = assoc[ tok_to_print[0] ]
						token = sentence[ token_idx ]
						field = token[ tok_to_print[1] ]
						args.append (field)
					
					fout.write ( str_format.format (*args) )
					fout.write ("\n")
					
	def header ( self, header ):
		self.header_str = header
		return self
	
	def sentence_separator_line ( self, sentence_separator_line ):
		self.sentence_separator_line_str = sentence_separator_line
		return self
	
	
	def __repr__ (self):
		'''
		  returns a pretty formatted string representation of self
		'''
		return "QueryResult with {} sentences".format(len(self.results)) + "\n" +\
		       "\n".join ([ repr(r) for r in self.results ])

class AsyncQueryResult:
	'''
	  Objects of class AsyncQueryResult contains an handler to the list of occurrencies of ConLL tokens that will be matched by a query in a Corpus.
	  
	  The results can be converted and outputted in various formats inculding CoNLL, raw text, csv.
	  Tokens that compose the results can be excluded or included from the output. Every attribute of each token can be included or excluded from the output.  
	  The results can be also used by one or many custom user-defined functions.
	  
	  Due to the asyncronous nature of this class, 
	   (1) the files on which the results have to be written and
	   (2) the functions that will use the results
	  must be specified before the results are estracted from the corpus, by calling the appropriate functions.
	  When all the wanted actions are specified, the extraction process can be started (see the Corpus documentation for more details).
	  The results will be used in every specified actions, and CANNOT BE REUSED later. 
	  
	  Attributes of the tokens composing AsyncQueryResult objects can be given in input to another query.
	
	'''
	
	def __init__ ( self, original_query, mapping ):
		'''
		  Builds an empty object, starting from a query (i.e. a tree of TokenShape) and the mapping between field names and CoNLL column indexes.
		
		'''
		self.original_query = original_query
		self.mapping = mapping
		self.token_sep = "\t"
		self.field_sep = "_"
		self.header_str = None
		self.sentence_separator_line_str = None
		
		all_the_tokenshapes = CompiledTokenShape.tree_to_set ( CompiledTokenShape.find_root (original_query) )
		self.tokens_to_print = [ (tok,["LEMMA","UPOSTAG"]) for tok in list ( all_the_tokenshapes ) ]
		self.last_matched_sentence = None
		
		self.scheduled_actions = []
	
	def write_to_conll_file (self, output_filename, remove_duplicates=True):
		'''
		  Outputs the query results represented by the self object to a CoNLL file called output_filename.
		  If remove_duplicates is True and more than one matches are found in the same sentence, that sentence is printed only once. This is the default behaviour.
		
		  the actual writing will be performed when the extracting process starts.
		'''
		fout = open (output_filename, "w")
		paramlist = [ fout, remove_duplicates ]
		self.scheduled_actions.append ((self.async_write_to_conll_file, paramlist))
		
	def async_write_to_conll_file ( self, matched, fout, remove_duplicates ):
		'''
		  performs the actual writing of a single mathced result on a CoNLL file
		'''
		
		if remove_duplicates and matched is self.last_matched_sentence:
			return
		
		sentence = matched.sentence
		fout.write ( "\n".join( ["#"+comment for comment in sentence.comments] ) )
		fout.write ("\n")
		fout.write ( "\n".join( ["\t".join(tok) for tok in sentence.tokens] ) )
		fout.write ("\n")
		
	
	def write_to_raw_text_file ( self, output_filename, remove_duplicates=True, form_col=None ):
		'''
		  outputs the query results represented by the self object to a raw text file.
		  Every single sentence that matched the query is printed on a single line of the file.
		  If remove_duplicates is True and more than one matches are found in the same sentence, that sentence is printed only once. This is the default behaviour.
		  
		  the actual writing will be performed when the extracting process starts.
		'''
		form_col = form_col if form_col is not None else self.mapping["FORM"] 
		fout = open (output_filename, "w")
		paramlist = [ fout, remove_duplicates, form_col ]
		self.scheduled_actions.append ((self.async_write_to_raw_text_file, paramlist))
		
	
	def async_write_to_raw_text_file ( self, matched, fout, remove_duplicates, form_col ):
		'''
		  performs the actual writing of a single mathced result on a CoNLL file
		'''
		
		if remove_duplicates and matched is self.last_matched_sentence:
			return
		
		sentence = matched.sentence
		fout.write ( " ".join( [tok[form_col] for tok in sentence.tokens] ) )
		fout.write ("\n")
		
	
	def write_columns ( self, output_filename, tokens_to_print=None, field_separator="_", token_separator="\t" ):
		'''
		  Outputs some of the tokens mathced by the query on a file, one per columns.
		  With the default behaviour, this function can be used to get a tsv (tabulation separated values) file, but the default column separator can be changed to get a csv (comma separated values), or to any other character to get an any-other-character separated values file.
		  
		  For every token, one or more fields can be printed in the same column. Fields of the same token are separated by rhe field_separator string, which can be configured and defaults to _ (the underscore characters).
		  Particular attention must be payed to the possibility that the field_separator or token_separator appear in a field, in that case the output will be ambiguous.
		  
		  The list of tokens to print can be specified with the parameter token_to_print, which is a list of (TokenShape, [list of fields]) tuple.
		   The first element of each tuple specifies which token have to be printed, and must be a TokenShape of the original query.
		   The second element of each tuple specifies which fields of that token have to be printed.
		   
		  The actual writing will be performed when the extracting process starts. 
		'''
		
		if tokens_to_print is None:
			tokens_to_print = self.tokens_to_print
		if field_separator is None:
			field_separator = self.field_sep
		if token_separator is None:
			token_separator = self.token_sep
		
		try:
			tokens_to_print_compiled = [ (tok[0], [self.mapping[field] for field in tok[1] ] ) for tok in tokens_to_print ]
		except KeyError:
			error_msg = "[BAD USER INPUT] the specified list of tokens to print is not valid" + "\n" + \
						"                 list: {}".format (tokens_to_print) + "\n" +\
						"                 MAPPING: {}".format ( self.mapping ) + "\n" +\
						"                 note that the list above may be the default list of tokens." +"\n" +\
						"                 in that case, please provide a valid list as argument to the function write_columns" + "\n"
			raise KeyError (error_msg)
		
		fout = open (output_filename, "w")
		paramlist = [ fout, tokens_to_print_compiled, field_separator, token_separator ]
		self.scheduled_actions.append ((self.async_write_columns, paramlist))
		
		if self.header_str is not None:
			fout.write (self.header_str)
			fout.write ("\n")
		
		
	def async_write_columns ( self, matched, fout, tokens_to_print_compiled, field_separator, token_separator ):
		'''
		  performs the actual writing of a single mathced result on a column separated file
		'''
		
		sentence = matched.sentence.tokens
		assocs_list = matched.associations
		
		if self.sentence_separator_line_str is not None:
			fout.write (self.sentence_separator_line_str)
			fout.write ("\n")
		
		for assoc in assocs_list:
			first = True
			for tok_to_print in tokens_to_print_compiled:
				if first:
					first = False
				else:
					fout.write ( token_separator )
				
				token_idx = assoc[ tok_to_print[0] ]
				token = sentence[ token_idx ]
				fields = [ token[i] for i in tok_to_print[1] ]
				
				fout.write ( field_separator.join (fields) )
				
			fout.write ("\n")
					
	def write_custom_format ( self, output_filename, str_format, *tokens_to_print ):
		'''
		  Outputs some of the tokens matched by the query, one match per line. The format of the lines can be specified with the parameter str_format.
		  
		  Any character in str_format is copied in the output file, except the substring "{}", that is replaced by one field of a matched token.
		  When writing the output for every single match, the first pair of curly braces {} found in str_format is replaced with the first field specified in the tokens_to_print list.
		   then, the second pair of curly braces is replaced with the second field specified in the tokens_to_print list and so on, until no pair of curly braces remains in str_format.
		  
		  The tokens_to_print must be a list of (TokenShape, field) tuple, where:
		   the first element of each tuple specifies the token to print, and must be a TokenShape from the original query.
		   the second element of each tuple specifies which field of that token must be printed.
		  
		  If more than one line per match is needed, just include the "\n" character in the str_format.
		  In addiction to the simple curly braces substitution, this function fullly supports the standard python's format syntax: https://docs.python.org/3.6/library/string.html#format-string-syntax
		  
		  The actual writing will be performed when the extracting process starts.
		'''	
			
		
		try:
			tokens_to_print_compiled = [ (tok[0], self.mapping[tok[1]] ) for tok in tokens_to_print ]
		except KeyError:
			error_msg = "[BAD USER INPUT] the specified list of tokens to print is not valid" + "\n" + \
						"                 list: {}".format (tokens_to_print) + "\n" +\
						"                 MAPPING: {}".format ( self.mapping ) + "\n"
			raise KeyError (error_msg)
		
		fout = open (output_filename, "w")
		paramlist = [ fout, str_format, tokens_to_print_compiled ]
		self.scheduled_actions.append ((self.async_write_custom_format, paramlist))
		
		if self.header_str is not None:
			fout.write (self.header_str)
			fout.write ("\n")
		
		
	def async_write_custom_format ( self, mathced, fout, str_format, tokens_to_print_compiled ):
		'''
		  performs the actual writing of a single mathced result on a custom format file
		'''
		
		sentence = matched.sentence.tokens
		assocs_list = matched.associations
		
		if self.sentence_separator_line_str is not None:
			fout.write (self.sentence_separator_line_str)
			fout.write ("\n")
		
		for assoc in assocs_list:
			
			args = []
			for tok_to_print in tokens_to_print_compiled:
				token_idx = assoc[ tok_to_print[0] ]
				token = sentence[ token_idx ]
				field = token[ tok_to_print[1] ]
				args.append (field)
			
			fout.write ( str_format.format (*args) )
			fout.write ("\n")
					
	def header ( self, header ):
		'''
		  Sets a header to use as first row when outputting to a file
		'''
		self.header_str = header
		return self
	
	def sentence_separator_line ( self, sentence_separator_line ):
		'''
		  Sets a line that will be printed to separate every matched sentence 
		'''
		self.sentence_separator_line_str = sentence_separator_line
		return self
	
	def execute_custom_function ( self, fun, *params ):
		'''
		  Executes a custom function once for every sentence that matched the query in the corpus.
		  The function is executed as in:
			
			fun ( matched, *params )
		  
		  where fun is the function given as parameter
		    matched is a MatchedSentence that represents the current matched sentence
		    *params are all the other parameters that are given as arguments to this function, passed as they are (the params argument).
		 
		  The actual execution will be performed when the extracting process starts. 
		'''
		
		self.scheduled_actions.append ((fun, params))
	
	def perform ( self, matched ):
		'''
		  Performs all the scheduled actions (file writing, custom function executing) on a matched sentence
		'''
		
        for fun, paramlist in self.scheduled:
            fun (matched, *paramlist)
           
        self.last_matched_sentence = matched
            
            
	def __repr__ (self):
		'''
		  returns a pretty formatted string representation of self
		'''
		return "QueryResult with {} sentences".format(len(self.results)) + "\n" +\
		       "\n".join ([ repr(r) for r in self.results ])

class MatchedSentence:
	
	def __init__ (self, sentence, associations ):
		self.sentence = sentence
		self.associations = associations
	
	def __repr__ ( self ):
		return "sentence: {}".format ( self.sentence ) +"\n" +\
		       "with associations {}".format (self.associations)

#TODO: use itertools.product instead
def all_the_permutations ( list_of_choices, idx=0 ):
	'''
	  given a list of lists (choices), returns a list containing all the possible sequences formed by taking in position i, every possible element for the list i
	
	  for example, if list_of_choices is [ [1,2], [3], [4,5,6] ]
	   the return value will be: [ [1,3,4],
	                               [1,3,5],
	                               [1,3,6],
	                               [2,3,4],
	                               [2,3,5],
	                               [2,3,6] ]
	                               
	   only lists with index greater than or equal to idx are considered.
	   
	'''
	
	if idx == len (list_of_choices) - 1:
		return [ [choice] for choice in list_of_choices[idx] ]
	else:
		ret = []
		permutataion_of_following_elements = all_the_permutations ( list_of_choices, idx + 1 )
		
		for perm in permutataion_of_following_elements:
			for el in list_of_choices[idx]:
				ret.append ( [el] + perm )
		return ret

class CompactChoiceNode:
	
	def __init__ (self):
		self.possible_choices = []
		
	def add_choice ( self, choice ):
		self.possible_choices.append (choice)
	
	def expand ( self ):
		'''
		  :return: list of dicts
		'''
		choices_expanded = [ choice.expand() for choice in self.possible_choices ]
		return [ item for sublist in choices_expanded for item in sublist ]
	
	def empty ( self ):
		return len(self.possible_choices) == 0

class CompactMatchedSentence:
	
	def __init__ ( self, root, to_be_expanded ):
		self.root = root
		self.to_be_expanded = to_be_expanded
	
	def expand (self):
		'''
		  :return: list of dicts
		'''
		if self.to_be_expanded is None:
			return [ { self.root[0]: self.root[1] } ]
		
		res = []
		expanded_children = [ child.expand() for child in self.to_be_expanded ]
		for perm in all_the_permutations ( expanded_children ):
			choice = { self.root[0]: self.root[1] }
			for d in perm:
				choice.update (d)
			res.append ( choice )
		
		return res
	
	def __repr__ (self):
		to_be_expanded_string = "" if self.to_be_expanded is None else " ->" + repr(self.to_be_expanded)
		return repr(self.root) + to_be_expanded_string

#unit tests
if __name__ == "__main__":
	list_of_choices = [ [1,2], [3], [4,5,6] ]
	print ("all the possible permutation choosing from {}".format(list_of_choices))
	print ( all_the_permutations ( list_of_choices ) )
	
	list_of_choices = [ [1,2], [3], [4,5,6], [7], [8], [9, 0] ]
	print ("all the possible permutation choosing from {}".format(list_of_choices))
	print ( all_the_permutations ( list_of_choices ) )
