#
# example1.py
# searches all the tree rooted in a verb (the TokenShape a) which has at leas an object (the TokenShape d)
#  also, there must be a sub-tree of a formed by a conjunction (the tokenShape b) and something that must not be a noun (the TokenShape c)
# in the corpus file "corpora/repubblica_sample".
# Then, it writes the results in CoNLL format into the file "example1_results.conll" and as raw text into the file "example1_results.txt" (one sentence per line)
#
# graphically, it searches :
#
#                          a (must be a verb)
#                         / \                            
#                        /   \                           
#                      obj    \                            
#                      /       \                         
#                     /         \                        
#                    d           b (must be a conjunction)                       
#                                |                      
#                                |                      
#                                |                      
#                                |                      
#                                c (must not be a noun)
#
#


from TokenShape import TokenShape
from Corpus import Corpus

if __name__ == "__main__":
	
	# Corpus object creation
	corpus = Corpus (open ("../corpora/repubblica_sample"), sentence_separator_line="</s>\n", comment_line_character="<" )
	
	# TokenShape definition:
	#  a must have UPOSTAG=V (it must be a verb)
	#  b must be a conjunction
	#  c must not be a noun
	#  d can be everything
	a = TokenShape ( specified_attributes_dict={"UPOSTAG":"V"} )
	b = TokenShape ( specified_attributes_dict={"UPOSTAG":"E"} )
	c = TokenShape ( negated_attributes_dict={"UPOSTAG":"S"} )
	d = TokenShape ( )
	
	# tree structure definition:
	#  c depends on b, that depends on a.
	#  d depends on a, and the relation type must be "obj"
	c.depends ( b )
	b.depends ( a )
	d.depends ( a, "obj" )
	
	# executing the query
	qr = corpus.find (b)
	
	# writing the results 
	qr.write_to_conll_file ( "example1_result.conll" )
	qr.write_to_raw_text_file ( "example1_result.txt" )
