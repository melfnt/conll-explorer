#!/usr/bin/python3

import re

class CompiledTokenShape:
	'''
	  Objects of class CompiledTokenShape have the same functionality of TokenShape objects, but they are indepent from the tagset and from the CoNLL standard used for the input file.
	  
	  CompiledTokenShape objects obtained from a set of TokenShape objects and from a Corpus object, resolving the mapping from attribute names to field indexes.
	  When a TokenShape is searched in a Corpus, its corresponding CompiledTokenShape is generated.
	  
	'''
	
	def __init__ ( self, specified_attributes_dict={}, negated_attributes_dict={}, regex_attributes_dict={} ):
		'''
		  The parameters are the same of TokenShape.__init__, but these dictionaries' keys are indexes that represent columns in a CoNLL file.
		  Values of regex_attributes_dict must be compiled regular expressions.
		  
		  For example, in the listing
			
			a = CompiledTokenShape ( specified_attributes_dict={2: "eat"} )
			b = CompiledTokenShape ( specified_attributes_dict={2: "eat"}, negated_attributes_dict={3: "VB"} )
			c = CompiledTokenShape ( regex_attributes_dict={2: re.compile("^.*ble$")} )
		  
		  The CompiledTokenShape a will match all the tokens whose column 2 is "eat".
		  The CompiledTokenShape b will match all the tokens whose column 2 is "eat" and whose column 3 is not "VB".
		  The CompiledTokenShape c will match all the tokens whose column 2 ends with "ble".
		  
		'''
		
		self.specified_attributes_dict = specified_attributes_dict
		self.negated_attributes_dict = negated_attributes_dict
		self.regex_attributes_dict = regex_attributes_dict
		self.parent = None
		self.children = {}
	
	def match ( self, conll_field_list ):
		'''
		  Checks if self matches a CoNLL Token, according to the rules for matching specified, negated and regex fields.
		  This function is not affected by the specified relationships of self (neither children nor parent).
		  
		  returns true iff self matches the parameter, i.e.:
		   for all the specified fields of self, the value of that field in the conll token is the same of the specified one.
		   for all the negated fields of self, the value of that field in the conll token is everything but the same of the specified one.
		   for all the regex-specified fields of self, the value of that field in the conll token matches the specified regex.	
		'''
		
		#~ print ("[DEBUG] try matching self: {}".format (self.specified_attributes_dict))
		#~ print ("               with conll: {}".format (conll_field_list))
		
		for field, value in self.specified_attributes_dict.items():
			if conll_field_list[field] != value:
				return False
		
		for field, value in self.negated_attributes_dict.items():
			if conll_field_list[field] == value:
				return False
		
		for field, value in self.regex_attributes_dict.items():
			if not value.match(conll_field_list[field]):
				return False
		
		return True		

	#~ def __repr__ (self):
		#~ parent_string = "None" if self.parent is None else id (self.parent) 
		#~ return "id:" + str (id(self)) + "\n" + \
			   #~ "specified: {}".format (self.specified_attributes_dict) + "\t" + \
			   #~ "negated: {}".format (self.negated_attributes_dict) + "\t" + \
			   #~ "regex: {}".format (self.regex_attributes_dict) + "\t" + \
			   #~ "parent: " + str(parent_string) + "\t" + \
			   #~ "children: {}".format ({id(child): self.children[child] for child in self.children}) + "\n"

	def __repr__ (self):
		return str(id(self))

def tree_to_set ( root ):
	'''
	  given a CompiledTokenShape root, returns the set of all the nodes contained the subtree of root, including root itself.
	'''
	
	current_set = set ([root])
	for child in root.children:
		current_set.update ( tree_to_set ( child ) )
	
	return current_set

def find_root ( node ):
	'''
	  given a CompiledTokenShape or TokenShape node, returns the root of the tree to which that node belongs.
	'''
	root = node.parent
	while root is not None:
		node = root
		root = node.parent
		
	return node
	

class QueryTree:
	'''
	  Represent a tree of CompiledTokenShape, matchable against a CoNLL sentence.
	  A CoNLL sentence s matches the tree if and only if there exists a tree formed by the dependencies of the CoNLL tokens in s that have the same structure of the query tree, and for each CoNLL token in s it matches the corresponding CompiledTokenShape in the query tree.
	  
	  A single CoNLL sentence can match a query in more than one way.
	  
	'''
	
	def __init__ ( self, tree_of_CompiledTokenShape ):
		self.root = find_root (  tree_of_CompiledTokenShape )
		self.CompiledTokenShape_involved_in_this_query = tree_to_set ( self.root )
	
	def __repr__ ( self ):
		return "QueryTree" + "\n" +\
			   " root: {}".format (self.root) + "\n" +\
			   " ids of all the CompiledTokenShape involved in this query: {}".format([ id(x) for x in self.CompiledTokenShape_involved_in_this_query ])

#unit tests
if __name__ == "__main__":
	
	a = CompiledTokenShape ( specified_attributes_dict={2: "eat"} )
	b = CompiledTokenShape ( specified_attributes_dict={2: "eat"}, negated_attributes_dict={3: "VB"} )
	c = CompiledTokenShape ( regex_attributes_dict={2: re.compile("^.*ble$")} )
	
	print ("a:")
	print (a)
	print ("b:")
	print (b)
	print ("c:")
	print (c)
	
	conll_sentence_string = "1	Mario	mario	NN	NNP	_	2	SBJ	_	_" + "\n" + \
						    "2	eats	eat	VB	VBZ	_	0	ROOT	_	_" + "\n" + \
						    "3	a	a	DT	DT	_	4	NMOD	_	_" + "\n" + \
						    "4	eat	eat	NN	NN	_	2	OBJ	_	_" + "\n" +  \
						    "5	in	in	IN	IN	_	4	NMOD	_	_" + "\n" + \
						    "6	an	a	DT	DT	_	8	NMOD	_	_" + "\n" + \
						    "7	adorable	adorable	JJ	JJ	_	8	NMOD	_	_" + "\n" + \
						    "8	way	way	NN	NN	_	5	PMOD	_	_" + "\n" + \
						    "9	.	.	.	.	_	2	P	_	_"
	
	for tok in conll_sentence_string.split ("\n"):
		tok_split = tok.split()
		print ( "Conll token: {}".format(tok) )
		print ( "a matches: {}".format (a.match(tok_split)) )
		print ( "b matches: {}".format (b.match(tok_split)) )
		print ( "c matches: {}".format (c.match(tok_split)) )
