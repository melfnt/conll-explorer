#
# example3.py
# performs a query over an italian corpus, searching:
#  - all the v-pp i.e. all the triplets formed by a noun, that depends on a conjunction, that depends on a verb, and the subject of the verb.
#
# here is a graphical representation of the tree of a v-pp with its subject:
#
#           v (must be a verb)
#          / \     
#         /   \
#       subj   \
#       /       \    
#      u         e (must be a conjunction)
#                |
#                |
#                s (must be a noun)
#
# then, it writes the result in a custom tsv format, writing one line for each vpp found;
# the line has the following format: 
#      {}_{}        {}_{}        {}-{}_{} 
# where every couple of character '{}' found is replaced by one of the field of a token, as shown in this picture:
#  
#  deprel of u            deprel of e
#    |                         |
#    |                         |
#    |        pos of v         |  lemma of s  
#    |            |            |     |
#    v            v            v     V
#     
#    {}_{}        {}_{}        {}-{}_{} 
#  
#        ^           ^             ^
#        |           |             |
#        |      lemma of v         |
#        |                         |
#        |                         |
#  lemma of u                  lemma of e
#
# That means that (on the first line of the output) the first {} is replaced with the deprel of u, then a _ is printed, then the lemma of u is printed, and so on.
# The same string with proper substitution is printed in the second line of the output file for the second vpp found, and so on for every vpp in the input file.
#
# For example, if the following sentence is in the corpus:
#                    "Il comando Home page permette il collegamento con la home page della biblioteca, nel caso la biblioteca la possegga."
#                    "The homepage command allows the connection with the library homepage, in case of the library has one."
# a vpp with subject is found:
#       v is "permettere" (to allow)
#       e is "in" (in)
#       s is "caso" (case) 
#       u is "comando" (command)
# this vpp produces the following line in the output file:
#                    subj_comando        V_permettere        comp-in_caso
#
#


from TokenShape import TokenShape
from Corpus import Corpus

if __name__ == "__main__":
	
	# Corpus object creation
	corpus = Corpus (open ("../corpora/itwac_sample"), sentence_separator_line="#\n", comment_line_character="#"  )
		
	# TokenShape definition for the third query: v must be a verb, e must be a conjunction and s must be a noun
	v = TokenShape ( specified_attributes_dict={"UPOSTAG":"V"} )
	e = TokenShape ( specified_attributes_dict={"UPOSTAG":"E"} )
	s = TokenShape ( specified_attributes_dict={"UPOSTAG":"S"} )
	u = TokenShape ()
	
	# tree structure definition for the third query: s depends on e that depends on v
	# u must depend on v, and the dependency type must be "subj"
	s.depends ( e )
	e.depends ( v )
	u.depends ( v, "subj" )
	
	# executing the query
	results = corpus.find ( v )
	
	# writing the results on a custom tsv format 
	results.header("extracted V-PP").sentence_separator_line("#"). \
			write_custom_format ( "example3_results.tsv", "{}_{}\t{}_{}\t{}-{}_{}", (u,"DEPREL"), (u,"LEMMA"), (v,"UPOSTAG"), (v,"LEMMA"), (e,"DEPREL"), (e,"LEMMA"), (s,"LEMMA") )
	
