#!/usr/bin/python3

import CompiledTokenShape
import re

def string_keys_to_indexes ( original_dict, mapping ):
	'''
	  returns a new_dictionary such that, for each pair (key, value) in the original_dict, it exists a pair (mapping[key], value) in the new_dictionary.
	  
	  raises a KeyError iff a key of the original_dict is not in the mapping
	
	'''
	return {mapping[k]: v for k,v in original_dict.items()} 

def compile ( ts, mapping, already_compiled=set() ):
	'''
	
	  returns a CompiledTokenShape given a mapping between field names and CoNLL column indexes.
	  also compiles regular expressions specified in ts.regex_attributes_dict 
	  
	  Raises a KeyError iff a CoNLL attribute of ts is not in the mapping.
	  
	  Recursively compiles parent and childrens iff they are not None and they are not in already_compiled  
	'''

	if ts is None or ts in already_compiled:
		return None
	
	already_compiled_and_ts = already_compiled | set ( [ ts ] )
	
	try:
		compiled_specified_attributes_dict = string_keys_to_indexes ( ts.specified_attributes_dict, mapping )
		compiled_negated_attributes_dict = string_keys_to_indexes ( ts.negated_attributes_dict, mapping )
		compiled_regex_attributes_dict = string_keys_to_indexes ( ts.regex_attributes_dict, mapping )
	except KeyError:
		error_msg = "[BAD USER INPUT] a field was not specified in the mapping between field names and CoNLL columns" + "\n" + \
		            "                   token: {}".format (ts) + \
		            "                   MAPPING: {}".format ( mapping ) + "\n"
		raise KeyError (error_msg)
	
	compiled_regex_attributes_dict_with_compiled_regex = {k: re.compile(v) for k, v in compiled_regex_attributes_dict.items()}
		
	ret = CompiledTokenShape.CompiledTokenShape ( compiled_specified_attributes_dict, compiled_negated_attributes_dict, compiled_regex_attributes_dict_with_compiled_regex )
	ret.source = ts
	
	compiled_parent = compile ( ts.parent, mapping, already_compiled_and_ts )
	if compiled_parent is not None:
		compiled_parent.children[ret] = ts.parent.children[ts]
	ret.parent = compiled_parent
	
	for child, relation in ts.children.items():
		compiled_child = compile ( child, mapping, already_compiled_and_ts )
		if compiled_child is not None:
			compiled_child.parent = ret
			ret.children[compiled_child] = relation
	
	return ret
	

class TokenShape:
	'''
	  Objects of class TokenShape have the same attributes of CoNLL tokens, except that some of them can be left unspecified or negated or they can be specified as a regular expression.
	  Objects of class TokenShape can be pattern-matched against CoNLL tokens.
	  The comparison between a CoNLL token ct and a TokenShape ts follows some special rules explained below:
	   these rules are applied for each attribute A of the tokens. We denote with ct.A the atribute A of the CoNLL token, and with ts.A the attribute A of the TokenShape. 
	   1 - if ts.A is specified,                            the comparison on A succeeds iff ct.A == ts.A
	   2 - if ts.A is unspecified,                          the comparison on A always succeedes
	   3 - if ts.A is negated,                              the comparison on A succeeds iff ct.A != ts.A
	   4 - if ts.A is specified as a regular expression r,  the comparison on A succeeds iff ct.A matches r
	  The comparison between tokens succeeds iff the comparison between all the attributes succeed.
	  
	  Objects of class TokenShape can have the same dependency structure that CoNLL tokens have in a corpus: ecery TokenShape can depends from another TokenShape.
	  Like attributes, the presence of a dependency can be specified or left unspecified. The type of the dependency (relation) can also be specified or left unspecified.
	  The comparison between a CoNLL token ct and a TokenShape ts follows some special rules we explain below:
	   1 - if the dependency is unspecified for ts,                                                    the comparison always succeedes
	   2 - if the dependency is specified from ts to ts1, but the type of dependency is not specified, the comparison succeedes iff ct depends on a token that matches ts1
	   2 - if the dependency is specified from ts to ts1, and the type of dependency is T,             the comparison succeedes iff ct depends on a token that matches ts1, and the type of dependency is T.
	  
	  Every specified, negated or regex-specified attributes and the presence of a dependency add some costraint to the TokenShape.
	  The tokenShape ts matches a CoNLL ct token iff ct satisfies every constraint of ts. 
	  
	  Attributes can be specified with the same names of the CoNLL fields specified in the mapping of the Corpus object in which the TokenShape will be searched,  
	                           or by the indexes of thier column in the CoNLL file
	  
	'''

	def __init__ ( self, specified_attributes_dict={}, negated_attributes_dict={}, regex_attributes_dict={} ):
		'''
		  Builds an object of class TokenShape.
		  The *_dict parameters are optional, and they must be in the form {"A": valA, "B": valB ...} where A,B... are the names of the attributes and valA, valB... are the values for A, B... respectively.
		   specified_attributes_dict, negated_attributes_dict and regex_attributes_dict indicates which attributes A of a token must be matched with, be different from or match the regex valA respectively.
		  All the attributes for which it doesn't exist a key in any of the three dictionaries are considered unspecified.
		  
		  For example, in the listing
			
			a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
			b = TokenShape ( specified_attributes_dict={"LEMMA": "eat"}, negated_attributes_dict={"UPOSTAG": "V"} )
			c = TokenShape ( regex_attributes_dict={"FORM": "^.*ble$"} )
		  
		  The TokenShape a will match all the tokens whose LEMMA is "eat".
		  The TokenShape b will match all the tokens whose LEMMA is "eat" and whose UPOSTAG is not "V".
		  The TokenShape c will match all the tokens whose FORM ends with "ble".
		  
		  If a TokenShape ts is build specifying an attribute as key of more than one dictionary, all the constraints implied by that attribute must be satisfies in order to match ts.
		   In other words, in order to match ts, a CoNLL token ct must match all the attributes specified in ts, 
		                                                   AND it must not match all the attributes negated in ts 
		                                                   AND it must regex-match all the attributes regex-specified in ts,
		   Regardless the fact that a key can be present in more than one dictionary.
		   Using such TokenShape for pattern-matching is often useless, for example it is nonsense to require a CoNLL token to have "LEMMA" equal to "eat" AND "LEMMA" not equal to "drink". (It is equivalent to require that its "LEMMA" is just equal to "eat").
		   
		'''
		self.specified_attributes_dict = specified_attributes_dict
		self.negated_attributes_dict = negated_attributes_dict
		self.regex_attributes_dict = regex_attributes_dict
		self.parent = None
		self.children = {}

	def specify ( self, attribute, value ):
		'''
		  Specifies that an attribute of the TokenShape self is equal to value. This attribute is added to the previous specified attributes, if any.
		  At query time, a CoNLL token will match self iff it satisfies all the previous constraint and its attribute is equal to value. 
		  
		  for example, in the listing:
		  
			  a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
			  a.specify ( "UPOSTAG", "V" )
		  
		  The TokenShape a will match all the tokens whose LEMMA is "eat" and whose UPOSTAG is "V".
		  
		  :return: self, for chaining
		
		'''
		self.specified_attributes_dict[attribute] = value
		return self
	
	def negate ( self, attribute, value ):
		'''
		  Negate that an attribute of the TokenShape self is equal to value. This attribute is added to the previous negated attributes, if any.
		  At query time, a CoNLL token will match self iff it satisfies all the previous constraint and its attribute is not equal to value. 
		  
		  for example, in the listing:
		  
			  a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
			  a.negate ( "UPOSTAG", "V" )
		  
		  The TokenShape a will match all the tokens whose LEMMA is "eat" and whose UPOSTAG is not "V".
		  
		  :return: self, for chaining
		
		'''
		self.negated_attributes_dict[attribute] = value
		return self
	
	def regex_specify ( self, attribute, value ):
		'''
		  Specified that an attribute of the TokenShape self matches the regular expression value. This attribute is added to the previous attributes specified as regular expressions, if any.
		  At query time, a CoNLL token will match self iff it satisfies all the previous constraint and its attribute matches the regular expression value. 
		  
		  for example, in the listing:
		  
			  a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
			  a.regex_specify ( "FEATS", "^.*per=3.*$" )
		  
		  The TokenShape a will match all the tokens whose LEMMA is "eat" and whose FEATS matches the specified regex (i.e. it is a third person token, use `gender` and `person` to gain control on morphology).
		
		  :return: self, for chaining
		  
		'''
		self.regex_attributes_dict[attribute] = value
		return self
	
	def depends ( self, head, relation=None ):
		'''
		  Specify that self must depend on another TokenShape head with some kind of relation. If relation is None or unspecified, it can be any relation.
		  At query time, a CoNLL token will match self iff it satisfies the previous constraints and it depends on a token that matches head.
		  The same costraint in the opposite direction is added to the TokenShape head.
		  
		  for example, in the listing:
		  
			  a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
			  b = TokenShape ( specified_attributes_dict={"LEMMA": "pizza"} )
			  b.depends ( a, "nsubj" )
		
		  The TokenShape a will match all the tokens whose LEMMA is "eat", and which have a dependant that matches b, with the relation "subj".
		  The TokenShape b will match all the tokens whose LEMMA is "pizza", and that depend on a token that matches a with the relation "subj" (i.e. all the tokens whose LEMMA is "pizza" and which are subject of any token which LEMMA is "eat" )
		  
		  Note that the call to this function will also modify head, adding to it the constraint that it must have a dependant which matches self.
		  
		  Since every CoNLL token must depends on at most one other CoNLL token, if this function is called multiple times, only the last one is considered. 
		  
		  Cyclic dependancy trees (i.e. a depends on b, that depends on c, that depends on d... that depends on a) are also forbidden, this will generate an error at query time.
		  
		  :return: self, for chaining
		
		'''
		if self.parent:
			try:
				self.parent.children.pop (self)
			except KeyError:
				err_msg = "[BUG] Trying to adding a parent to a TokenShape using TokenShape.depends" + "\n" + \
				          "      self already had an old parent, but self was not listed among the old parent children." + "\n" + \
				          "      self: {}".format (repr(self)) + "\n" + \
				          "      old parent: {}".format (repr(self.parent)) + "\n" + \
				          "      new parent: {}".format (repr(head)) + "\n" + \
				          "      type of relation: {}".format (relation) + "\n"
				raise KeyError ( err_msg )
		self.parent = head
		head.children[self] = relation
		return self
		
	#~ def __repr__ (self):
		#~ parent_string = "None" if self.parent is None else id (self.parent) 
		#~ return "id:" + str (id(self)) + "\n" + \
			   #~ "specified: {}".format (self.specified_attributes_dict) + "\t" + \
			   #~ "negated: {}".format (self.negated_attributes_dict) + "\t" + \
			   #~ "regex: {}".format (self.regex_attributes_dict) + "\t" + \
			   #~ "parent: " + str(parent_string) + "\t" + \
			   #~ "children: {}".format ({id(child): self.children[child] for child in self.children}) + "\n"
	def __repr__ (self):
		return str(id(self))
			
	
#unit tests
if __name__ == "__main__":
	
	a = TokenShape ( specified_attributes_dict={"LEMMA": "eat"} )
	b = TokenShape ( specified_attributes_dict={"LEMMA": "eat"}, negated_attributes_dict={"UPOSTAG": "V"} )
	c = TokenShape ( regex_attributes_dict={"FORM": "^.*ble$"} )
	
	c.specify ( "FORM", "incredible" )
	c.negate ( "UPOSTAG", "N" )
	c.regex_specify ( "FORM", ".*ble$" )
	
	a.depends (b)
	c.depends (a)
	c.depends (b)
	
	print ("a")
	print (a)
	print ("b")
	print (b)
	print ("c")
	print (c)
	
	d = TokenShape ( specified_attributes_dict={"LEMMA": "dog"} )
	e = TokenShape ( specified_attributes_dict={"LEMMA": "cake"} )
	f = TokenShape ( specified_attributes_dict={"LEMMA": "garden"} )
	
	d.depends ( a, "subj" )
	e.depends ( a, "obj" )
	f.depends ( a, "nmod" )
	
	mapping = {"LEMMA":3, "FORM":2, "UPOSTAG": 4, "FEATS":6}
	compiled_a = compile (a, mapping)
	
	print ("d")
	print (d)
	print ("e")
	print (e)
	print ("f")
	print (f)
	
	print ("Tree structure:")
	print ("b")
	print ("|a")
	print ("||d")
	print ("||e")
	print ("||f")
	print ("|c")
	
	print ("mapping: {}".format (mapping))
	
	print ("compiled a")
	print (compiled_a)
	
	print ("parent of compiled a")
	print (compiled_a.parent)
	
	print ("children of the parent of compiled a")
	print (compiled_a.parent.children)
	
	print ("children of compiled a")
	print (compiled_a.children)
	
	q = CompiledTokenShape.QueryTree ( compiled_a )
	print ("\n---\n")
	print ("QueryTree in which compiled a belong:")
	print (q)
	
