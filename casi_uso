
-------------------------------
| Libreria da usare in python |
-------------------------------

Apre un corpus per il parsing:
	C = corpus ( "/path/to/input/file.conll" )	

Trova tutti token che compaiono come soggetti di token com lemma "mangiare":
	tok_verb = Token ( lemma="mangiare" )
	tok_sogg = Token ( output=True )
	tok_sogg.depends ( tok_verb, "subj" )	
	
	results = C.find_all ( tok_sogg )

Trova tutti i token che compaiono come soggetti di token con lemma "mangiare". Restituisce sia i soggetti che i token con lemma "mangiare":
	tok_verb = Token ( lemma="mangiare", output=True )
	tok_sogg = Token ( output=True )
	tok_sogg.depends ( tok_verb, "subj" )
		
	results = C.find_all ( tok_sogg )

Trova tutti i token che compaiono come soggetti di token con lemma "mangiare". Restituisce i token con lemma "mangiare", i soggetti e i loro determinanti (se esistono):
	tok_verb = Token ( lemma="mangiare", output=True )
	tok_sogg = Token ( output=True )
	tok_sogg.depends ( tok_verb, "subj" )
	tok_det = Token ( output=True, optional=True )
	tok_det.depends ( tok_sogg, "det" )
	
	results = C.find_all ( tok_sogg )

Notare che al posto dell'ultima riga di codice si poteva usare:
	results = C.find_all ( tok_det )
oppure:
	results = C.find_all ( tok_verb )
il risultato sarebbe stato lo stesso. (/* TODO: opzioni di output */)

Trova tutti i token con lemma "mangiare". Restituisce i token trovati e tutti i token che dipendono da loro.
Notare che in output ci può essere lo stesso token più volte, per esempio nella frase "il gatto mangia il topo" l'output sarà {(mangia, gatto), (mangia,topo)}, il token "mangia" è lo stesso in entrambe le coppie
	tok_verb = Token ( lemma="mangiare", output=True )
	tok_relatum = Token ( output=True )
	tok_relatum.depends ( tok_verb )
	
	results = C.find_all ( tok_relatum )

Trova tutti i token con lemma "mangiare", per i quali esiste una sequenza di dipendenze, che finisce con un token che ha lemma "il".
Per esempio, nella frase "il cane veloce mangia", l'output è {(mangia)}, nella frase "Lucio mangia", l'output è {}

	tok_verb = Token ( lemma="mangiare", output=True )
	tok_relatum = Token ( lemma="il" )
	tok_relatum.rdepends ( tok_verb )
	
	results = C.find_all ( tok_relatum )

Notare che l'output contiene solo i token con lemma "mangiare" anche se si stanno cercando le occorrenze di tok_relatum. /*TODO: opzioni di output*/

Trova tutti i token con lemma "mangiare", per i quali esiste una sequenza di dipendenze, che finisce con un token che ha lemma "il". Restituisce tutti i token
Per esempio, nella frase "il cane veloce mangia", l'output è {(mangia)}, nella frase "Lucio mangia", l'output è {}

-----------------------------------
| Linguaggio parsato (tipo xpath) |
-----------------------------------

Trova tutti token che compaiono come soggetti di token com lemma "mangiare":
( lemma="mangiare" ) /subj/ (*)

Trova tutti i token che compaiono come soggetti di token con lemma "mangiare". Restituisce sia i soggetti che i token con lemma "mangiare":
( lemma="mangiare" * ) /subj/ (*)

Trova tutti i token che compaiono come soggetti di token con lemma "mangiare". Restituisce i token con lemma "mangiare", i soggetti e i loro determinanti (se esistono):
( lemma="mangiare" * ) /subj/ (*) /det/ (*?)

Trova tutti i token con lemma "mangiare". Restituisce i token trovati e tutti i token che dipendono da loro.
( lemma="mangiare" * ) // (*)

Trovare tutti i token con lemma "mangiare", per i quali esiste una sequenza di dipendenze, che finisce con un token che ha lemma "il".
( lemma="mangiare" * ) /// ( lemma="il" )

Mappa conll
https://arxiv.org/pdf/1710.01779.pdf
