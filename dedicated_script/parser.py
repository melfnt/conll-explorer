#!/usr/bin/python
#coding: utf-8

class Counter:
	def __init__ (self):
		self.diz = {}
	
	def add_instance ( self, pos, lemma ):
		if pos not in self.diz:
			self.diz[pos] = {}
		if lemma not in self.diz[pos]:
			self.diz[pos][lemma] = 0
		self.diz[pos][lemma] += 1
		
	def print_statistics ( self, pos, file_statistics ):
		with open (file_statistics, "w") as fw:
			fw.write (pos+"\n")
			for lemma in self.diz[pos]:
				print_str = lemma+"\t"+str(self.diz[pos][lemma])
				fw.write (print_str+"\n")

class Frase:
	
	def __init__ (self, counter):
		self.tokens = []
		self.counter = counter;
	
	def add (self, riga):
		token = riga.split ('\t')
		self.tokens.append (token)
		
	def estrai_frequenze ( self, pos ):
		for token in self.tokens :
			
			if token[3] == pos:
				self.counter.add_instance ( pos, token[2] )
	
	def estrai_frequenze_v_p_s ( self ):
		self.vpp = []
		for token in self.tokens:
			
			if token[3] == "S":
				id_padre = token[6]
				padre = self.tokens[int(id_padre)-1]
				if padre[3] == "E":
					id_nonno = padre[6]
					nonno = self.tokens[int(id_nonno)-1]
					if nonno[3] == "V":
						self.vpp.append ({"lemma":nonno[2],"preposizione":padre[2],"sostantivo":token[2]})
						self.counter.add_instance ( "V-PP", nonno[2]+"\t"+padre[2]+"\t"+token[2] )
		
	def filtra_verbi ( self, filtro ):
		for token in self.tokens :
			
			if token[3] == "V" and token[2] in filtro.verbi:
				self.counter.add_instance ( "VERB-FILTERED", token[2] )
	
	def filtra_vpp ( self, filtro ):
		for v in self.vpp:
			for filter_v in filtro.vpp:
				#~ print filter_v
				if filter_v["lemma"] == v["lemma"] and filter_v["preposizione"] == v["preposizione"] and filter_v["sostantivo"] == v["sostantivo"]:
					self.counter.add_instance ( "V-PP-FILTERED", v["lemma"]+"\t"+v["preposizione"]+"\t"+v["sostantivo"] )
				#~ m = raw_input ()
			
			 

_RIGA_FITTIZIA_PER_QUELLE_MANCANTI_NEL_FILE = "0	MANCANTE	MANCANTE	MANCANTE	MANCANTE	num=s	0	MANCANTE"

def check_id_token ( riga, id_token, line_no ):
	ret = True
	riga_split = riga.split ('\t')
	if not int (riga_split[0]) == id_token:
		print "Manca un numero alla linea",line_no-1,"sostituito con lemma e pos fittizi"
		#~ m = raw_input ()
		ret = False
	return ret

class Corpus_SST:
	
	def __init__ (self, file_corpus, counter, filtro ):
		self.frase = Frase ( counter )
		id_token = 1
		line_no = 1
		with open (file_corpus) as fr :
		
			for riga in fr:
				
				line_no += 1
				
				frase_stop = ( riga[0] == "#"  )
				if not frase_stop :
					while not check_id_token ( riga, id_token, line_no ):
						id_token += 1
						self.frase.add ( _RIGA_FITTIZIA_PER_QUELLE_MANCANTI_NEL_FILE )
						
					self.frase.add ( riga )
					
					id_token += 1
				else:
					self.v_p_s ()
					self.filtra_verbi ( filtro )
					self.filtra_vpp ( filtro )
					id_token = 1
					self.frase = Frase ( counter )	
			
	
	def v_p_s ( self ):
		self.frase.estrai_frequenze_v_p_s ()
		self.frase.estrai_frequenze ( "V" )
		self.frase.estrai_frequenze ( "S" )
	
	def filtra_verbi ( self, filtro ):
		self.frase.filtra_verbi ( filtro )
		
	def filtra_vpp ( self, filtro ):
		self.frase.filtra_vpp ( filtro )
		
			

class Survey:
	
	def __init__ ( self, file_survey ):
		self.verbi = set ()
		self.vpp = []
		with open (file_survey) as fr :
			for riga_con_a_capo in fr:
				riga = riga_con_a_capo[0:-1]
				riga_split = riga.split (" ")
				verb_underscore = riga_split[1]
				verb_split = verb_underscore.split("_")
				verb = verb_split[1]
				self.verbi.add (verb)
				comp_underscore = riga_split[-1]
				prep_and_noun = comp_underscore.split("-")[1]
				prep = prep_and_noun.split("_")[0]
				noun = prep_and_noun.split("_")[1]
				self.vpp.append ( {"lemma":verb,"preposizione":prep,"sostantivo":noun} )

class Repubblica:
	
	
	def __init__ ( self, file_repubblica, stats ):
		self.vpp = []
		
		self.stats = stats
		line_no = 0
		with open (file_repubblica) as fr :
			for riga in fr:
				
				line_no += 1
				if line_no % 10000 == 0:
					print "ho letto",line_no,"righe"
				
				riga_split = riga.split(" ")
				lemma = None
				preposizione = None
				sostantivo = None
				for tok in riga_split:
					if "lemma=" in tok:
						lemma = tok.split('"')[1].split("-")[0]
					if "comp_" in tok:
						preposizione = tok.split ("_")[1].split("=")[0]
						sostantivo = tok.split ('"')[1]
				self.add ( lemma, preposizione, sostantivo ) 
	
	def add ( self, lemma, preposizione, sostantivo ):
		if lemma is not None and sostantivo is not None and preposizione is not None and ";" not in sostantivo:
			sostantivo_split = sostantivo.split ("-")
			if len(sostantivo_split) == 2 and sostantivo_split[1] == "s":
				sostantivo = sostantivo.split("-")[0]
				self.vpp.append ({"lemma":lemma,"preposizione":preposizione,"sostantivo":sostantivo})
				self.stats.add_instance ( "V-PP", lemma+"\t"+preposizione+"\t"+sostantivo )
				self.stats.add_instance ( "VERB", lemma )
				self.stats.add_instance ( "NOUN", sostantivo )
	
	def filtra_verbi ( self, filtro ):
		for v in self.vpp:
			if v["lemma"] in filtro.verbi:
				self.stats.add_instance ( "VERB-FILTERED", v["lemma"] )
	
	def filtra_vpp ( self, filtro ):
		for v in self.vpp:
			for filter_v in filtro.vpp:
				if filter_v["lemma"] == v["lemma"] and filter_v["preposizione"] == v["preposizione"] and filter_v["sostantivo"] == v["sostantivo"]:
					self.stats.add_instance ( "V-PP-FILTERED", v["lemma"]+"\t"+v["preposizione"]+"\t"+v["sostantivo"] )
	
if __name__ == "__main__" :
	stats = Counter ()
	filtro = Survey ("survey")
	C = Corpus_SST ( "../corpora/itwac_sample", stats, filtro )
	stats.print_statistics ( "V", "frequenze_verbi" )
	stats.print_statistics ( "S", "frequenze_nomi" )
	stats.print_statistics ( "VERB-FILTERED", "frequenze_verbi_filtrati" )
	stats.print_statistics ( "V-PP", "frequenze_vpp" )
	stats.print_statistics ( "V-PP-FILTERED", "frequenze_vpp_filtrate" )
